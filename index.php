<?php 
/**
 * @author: Renan Miranda
 * @comments: 
 * @purpose: greetings and survey explanation
 */
session_start();

//includes
require_once('includes/redirect.php');

// print_r($_POST);
// print_r($_SERVER);

returnLastStage();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/materialize.css">
    <title>Survey</title>
</head>
<body>
   <div class="container">
    <h3>Hi! Thanks for taking the survey.</h3>
        <p>The purchase satisfaction survey is going to help us identify users habits and preferences. 
        Help us filling the inputs as follow! It won't take more than 5 minutes.</p>
        <div class="divider"></div>
        <blockquote>
        We appreciate your input.
        </blockquote>

        <form action="includes/wizard_1.php" method="get">
            <button class="waves-effect waves-light btn-large" type="submit">Start Survey</button>
        </form>

        <script src="js/materialize.js"></script>
   </div>
</body>
</html>
