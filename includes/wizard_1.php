<?php 
/**
 * @author: Renan Miranda
 * @comments: 
 * @purpose: wizard page 1, gathering user's information
 */
    session_start();

    //required
    require_once("session.php");
    require_once("redirect.php");

    $_SESSION["previous"] = "../index.php";
    $_SESSION["next"] = "wizard_2.php";
    setSession('current', 'includes/wizard_1.php');
   
?>
<!DOCTYPE html>
<html lang="en">
<?php
    // define variables and set to empty values
    $fullName = $age = $invalidFullNameClass = $invalidAgeClass = $invalidStudentOptionsClass = $student_options = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if ($_POST["submit"] == 'previous') {
            unset($_SESSION['current']);
            redirect($_POST["submit"]);
        }
        // print_r($_POST);
        // print_r("\n");
        // // print_r($_SESSION);
        // print_r("\n");
        // // print_r($_SERVER);

        $fullName = sanitize($_POST["full_name"]);
        $age = sanitize($_POST["age"]);

        if (isset($_POST["student_options"])) {
            $student_options = sanitize($_POST["student_options"]);
        }
        

        validate("full_name", $fullName);
        validate("age", $age);
        
        validate("student_options", $student_options);

        if (!empty($fullName) && !empty($age) && !empty($student_options)) {
            redirect($_POST["submit"]);
        }
        
    }

    //helps preserve the value if one field is invalid
    function getValue($field) {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            return $_POST[$field];
        }
    }

    function validate($field, $value) {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $invalid = "invalid";
            global $invalidFullNameClass;
            global $invalidAgeClass;
            global $invalidStudentOptionsClass;

            switch (trim($field)) {
                case "full_name":
                    if(empty($value)) $invalidFullNameClass = $invalid;
                    break;
                
                case "age":
                    if(empty($value) || !is_numeric($value) || intval($value) < 1) $invalidAgeClass = $invalid;

                    break;

                case "student_options":
                    if (empty($value)) $invalidStudentOptionsClass = $invalid;
                    break;
            }
            setSession($field, $value);
        }
    }

    function sanitize($data) {

        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/materialize.css">
    <title>Survey - Personal Info</title>
</head>
<body>

    <div class="container">
        
        <div class="card-panel">
            <h3>Personal Info</h3>
            <div class="divider"></div>

            <div class="row">
                <form class="col s12" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" >
                    <div class="row">
                        <div class="input-field col s6">
                            <input class="<?php echo $invalidFullNameClass;?>" type="text" name="full_name" id="full_name" value="<?php echo getSession("full_name");?>">

                            <label for="full_name">Full Name</label>
                            <span class="helper-text" data-error="Invalid Name"></span>
                        </div>
                        <div class="input-field col s6">
                            <input class="<?php echo $invalidAgeClass;?>" type="text" name="age" id="age" value="<?php echo getSession("age");?>">
                            <label for="age">Age</label>
                            <span class="helper-text" data-error="Invalid Age"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="student_options" id="student_options">
                                <option value="" disabled selected>Choose your option</option>
                                <option <?php if (getSession("student_options") == 'f') { ?>selected="true" <?php }; ?>value="f" >Yes, Full Time</option>
                                <option <?php if (getSession("student_options") == 'p') { ?>selected="true" <?php }; ?>value="p">Yes, Part-time</option>
                                <option <?php if (getSession("student_options") == 'n') { ?>selected="true" <?php }; ?>value="n">No</option>
                            </select>
                            <label>Are you a student?</label>
                            <span class="helper-text" data-error="Invalid Selection" style="color:red;"><?php echo $invalidStudentOptionsClass; ?></span>

                        </div>
                    </div>
                    <div class="row">
                        <button class="btn-large" type="submit" name="submit" value="previous">Previous</button>
                        <button class="btn-large" type="submit" name="submit" value="next">Next</button>
                    </div>
                </form>

            </div>
        </div><!-- card-panel-->
    </div><!-- container-->
    

    <script src="../js/materialize.js"></script>
    <script src="../js/script.js"></script>
</body>
</html>