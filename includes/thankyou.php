<?php 
/**
 * @author: Renan Miranda
 * @comments: displays a success message at the end
 * @purpose: Thanks the user for taking the survey
 */
session_start();

//includes
require_once('session.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/materialize.css">
    <title>Survey - Thank you!</title>
</head>
<body>
    
<div class="container">
    <h3>Hi <?php echo getSession('full_name'); ?>! Thanks for taking the survey.</h3>
        <p>Your entries are very valuable to us and we won't share any of your information. </p>
        <p>Instead, we are going 
        to analyze the inputs and try to offer to you a better service in your next purchase!</p>
        <div class="divider"></div>
        <blockquote>
        We appreciate your input.
        </blockquote>
   </div>

<script src="../js/materialize.js"></script>
<script src="../js/script.js"></script>
</body>
</html>