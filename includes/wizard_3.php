<?php 
/**
 * @author: Hyei-In SUh
 * @comments: 
 * @purpose: wizard page 1, gathering user's information
 */

session_start();

require_once("session.php");
require_once("redirect.php");

//page control
$_SESSION["previous"] = "wizard_2.php";
$_SESSION["next"] = "summary.php";
setSession('current', 'includes/wizard_3.php');


 //printSession();
//  print_r($_POST);
?>
<!DOCTYPE html>
<html lang="en">
    <pre>
    <?php 
        $satisfactErr = $recommendErr = "";
        $satisfaction = $recommendation = "";
        $errors = array();


        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            if ($_POST["submit"] == 'previous') {
                redirect('previous');
            }

           
            $devices = getSession('checkbox_purchase');

            for ($i=0; $i < count($devices); $i++) { 
                if (!isset($_POST['satisfactory'.$i])) {
                    $errors['satisfactory'.$i] = "You must select at least one of the options";
                   
                }
                
                // elseif(!isset($_POST['recommendation'.$i])){
                //     $errors['recommendation'.$i] = "you must select";
                    
                // }
            }
            $summary = array();

            if (count($errors) == 0) {
                // $devices = getSession('checkbox_purchase');
                for ($i=0; $i < count($devices); $i++) {
                    $key = $devices[$i];
                
                    $satisfactory = $_POST['satisfactory'.$i];
                    //$recommendation = $_POST['recommend_options'][$i]; 
                
                    //$_SESSION['satisfactory'.$i] = $satisfactory;

                    $summary[$key] = array("satisfaction"=>$satisfactory, "recommendation"=>$recommendation);
                }

    
                    $_SESSION['summarized'] = $summary;

                    //move to summary
                    redirect('next');
            }
             
        }

        function getError($index)
        {
            global $errors;
            if (isset($errors['satisfactory'.$index])) {                
                return $errors['satisfactory'.$index];
            }
            
            // if(isset($errors['recommendation'.$index])){
            //     return $errors['recommendation'.$index];
            // }
            
            return "";
        }    
    ?>
    </pre>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/materialize.css">
    <title>Survey - User Experience</title>
</head>
<body>

    <div class="container">

        <div class="card-panel">
            <h3>User Experience</h3>
            <div class="divider"></div>

            <div class="row">
                <form class="col s12" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" >

                <?php
                    
                    foreach (getSession('checkbox_purchase') as $key => $device) {
            
                        echo '<h5>'.$device.'</h5>';
                        echo '<div class="divider"></div>';
                        
                        echo '<div class="row">';
                        echo '    <div class="input-field col s12">';
                        echo '        <p>';
                        echo '        <label>How happy are you with this device on a scale from 1(not satisfied) to 5 (very satisfied)?</label>';
                        echo '        <span class="helper-text" style="color:red;" data-error="Invalid">'.getError($key).'</span>';
                        echo '        </p>';
                                
                        echo '        <p>';
                        echo '        <label>';
                        echo '            <input type="radio" name="satisfactory'.$key.'"  value="1" />';
                        echo '            <span>1</span>';
                        echo '        </label>';
                        echo '        </p>';

                        echo '        <p>';
                        echo '        <label>';
                        echo '            <input type="radio" name="satisfactory'.$key.'" value="2" />';
                        echo '            <span>2</span>';
                        echo '        </label>';
                        echo '        </p>';
                                
                        echo '        <p>';
                        echo '        <label>';
                        echo '            <input type="radio"  name="satisfactory'.$key.'" value="3" />';
                        echo '            <span>3</span>';
                        echo '        </label>';
                        echo '        </p>';
                               
                        echo '        <p>';
                        echo '        <label>';
                        echo '            <input type="radio"  name="satisfactory'.$key.'"  value="4" />';
                        echo '            <span>4</span>';
                        echo '        </label>';
                        echo '        </p>';
                              
                        echo '        <p>';
                        echo '        <label>';
                        echo '            <input type="radio"  name="satisfactory'.$key.'" value="5" />';
                        echo '            <span>5</span>';
                        echo '        </label>';
                        echo '        </p>    ';                 
                        echo '    </div>';
                        echo '</div>';

                        echo '<div class="row">';
                        echo '    <div class="input-field col s12">';
                        echo '        <select name="recommend_options[]" id="recommend_options">';
                        echo '            <option value="" disabled selected> </option>';
                        echo '            <option value="yes">Yes</option>';
                        echo '            <option value="maybe">Maybe</option>';
                        echo '            <option value="no">No</option>';
                        echo '        </select>';
                        echo '        <label>Would you recommend the purchase of this device to a friend?</label>';
                        echo '  <span class="helper-text" style="color:red;" data-error="Invalid"></span>';       
                       
                        echo '    </div>';
                        echo '</div>';
                    }


                ?>

                    <div class="row">
                            <button class="btn-large" type="submit" name="submit" value="previous">Previous</button>
                            <button class="btn-large" type="submit" name="submit" value="next">Next</button>
                    </div>
                </form>

            </div>
        </div><!-- card panel-->
    </div><!-- container-->
    

    <script src="../js/materialize.js"></script>
    <script src="../js/script.js"></script>
</body>
</html>