<?php
/**
 * @author: Hyeiin Suh
 * @comments: 
 * @purpose: session file, empowering session operations
 */

    function setSession($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    function getSession($key)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : '';
    }

    function printSession() {
        print_r("_SESSION=\n");
        print_r($_SESSION);
    }
?>

